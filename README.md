<h1 align= "center"> ingrownmink4.tumblr.com </h1>
<p align="center"> A lightweight and free/libre blog for Tumblr. Used as my personal website (at the moment).
<br>
<p align= "center"> <a href="https://github.com/fvsch/monospace-theme" rel="noopener"> Forked from Florent Verschelde's Monospace theme</a>. Tweaked by me with CSS. I added some other cool features too. </p>


### Built with
- HTML
- SCSS
- JavaScript

### Features
- No tracking
- No ads
- Content Security Policy enabled
- Multiple languages available (eu-ES, es-ES, en-EU)
- Dark mode enabled by default
- Compliant with the latest WCAG accessibility standards
- Much more…

 ![ingrownmink4.tumblr.com](lighthouseresults.png)

 ![ingrownmink4.tumblr.com](Captura%20desde%202022-07-23%2011-53-06-min-min.png)

 ![ingrownmink4.tumblr.com](firefox102ingrownminktumblr.png)
 
 ![ingrownmink4.tumblr.com](chromium103ingrownminktumblrv2.png)




